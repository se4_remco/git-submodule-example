#include <array>
#include <iostream>
#include <string>
#include <string_view>

inline bool isCool(std::string_view name)
{
    const std::array<std::string, 1> COOL_PEOPLE
    {
        "remmy"
    };

    const auto it = std::find_if(COOL_PEOPLE.cbegin(), COOL_PEOPLE.cend(), [&](const auto & s) {
        return s == name;
    });

    return it != COOL_PEOPLE.cend();
}
